# CI Build Version

Generates an opinionated [semantic version](http://semver.org/) from a base version and the current CI context. Currently supports GitLab CI. 

## Version Logic
The generated versions have the following [precendence](http://semver.org/#spec-item-11) (from highest to lowest) based on git ref:
1. Tag which is a semantic version  (release version).
  * It must match the base version (with no prerelease identifiers).
2. `release`/`hotfix` branches (prerelease starts with `rc`).
3. `master`/`develop` branches (prerelease starts with `dev`).
4. Other branches and tags (prerelease starts with `ci`).

If used outside a CI environment, the prerelease is always `local`.

## Usage
### TypeScript
```typescript
import { SemVer } from 'semver';
import generateBuildVersion, { load, loadSync, parse } from '@janslow/ci-build-version';

// Manually specify base version.
const buildVersion1: SemVer = generateBuildVersion(new SemVer('1.2.3-local'));

// Load base version from a file
const buildVersion2: Promise<SemVer> = load('package.json', 'UTF8');
const buildVersion3: SemVer = loadSync('package.json', 'UTF8');
const buildVersion4: SemVer = parse(require('package.json'));
```

### Command Line
```bash
ci-build-version -b 1.2.3-local # Generate a version using the base version 1.2.3-local
ci-build-version -f package.json # Generate a version using the base version in `package.json`
```

## Supported File Formats
### Plain text
Must only contain the base version. E.g.,

```
1.2.3-local
```

### JSON/YAML
Must either contain a JSON/YAML string which is the base version or an object with the key `version` (which is the base version). For example a [`package.json` file](https://docs.npmjs.com/files/package.json). YAML files must only contain a single document. E.g.,

```yaml
"0.1.2-local" # Base version
```

```json
{
  "version": "0.1.2-local"
}
```

```yaml
version: 0.1.2-local
```

---

## Development
### Debugging
```bash
export DEBUG='ci-build-version*'
```
