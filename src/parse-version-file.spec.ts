import { expect } from 'chai';
import { SemVer } from 'semver';
import YAML = require('yamljs');

import { parse } from './parse-version-file';

describe('parse-version-file', () => {
  describe('parse(raw)', () => {
    let toRaw: (version?: string) => string;

    function itThrowsAnErrorIfRawIs(description: string, ...rawValues: string[]): void {
      it(`throws an error if "raw" is ${description}`, rawValues.length === 0 ? undefined : () => {
        for (const raw of rawValues) {
          expect(() => parse(raw), `'${raw}'`).to.throw();
        }
      });
    }
    function itReturnsASemVerIfXIsAValidSemverString(x = 'the string'): void {
      it(`returns a SemVer ${x} is a valid semver string.`, () => {
        for (const version of ['1.2.3-local', '1.2.3', 'v1.2.3']) {
          const raw = toRaw(version);
          expect(parse(raw), `'${version}'`).to.deep.equal(new SemVer(version));
        }
      });
    }
    function itThrowsAnErrorIfXIsNotAValidSemver(x = 'the string'): void {
      it(`throws an error if ${x} is not a valid semver.`, () => {
        for (const version of ['foo', '1.2', '1.2.3.4', '1.2.RELEASE', undefined]) {
          const raw = toRaw(version);
          expect(() => parse(raw), `'${raw}'`).to.throw();
        }
      });
    }

    describe('when "raw" is a plain string', () => {
      beforeEach(() => toRaw = x => x === undefined ? '' : x);
      itReturnsASemVerIfXIsAValidSemverString();
    });
    describe('when "raw" is a JSON string', () => {
      beforeEach(() => toRaw = x => JSON.stringify(x));
      itReturnsASemVerIfXIsAValidSemverString();
      itThrowsAnErrorIfXIsNotAValidSemver();
    });
    describe('when "raw" is a JSON object', () => {
      beforeEach(() => toRaw = version => JSON.stringify({ version }));
      itReturnsASemVerIfXIsAValidSemverString('the "version" key');
      itThrowsAnErrorIfXIsNotAValidSemver('the "version" key');
    });
    describe('when "raw" is a YAML string', () => {
      beforeEach(() => toRaw = x => `# A comment\n${YAML.stringify(x)} # Another comment`);
      itReturnsASemVerIfXIsAValidSemverString();
      itThrowsAnErrorIfXIsNotAValidSemver();
    });
    describe('when "raw" is a YAML object', () => {
      beforeEach(() => toRaw = version => YAML.stringify({ version }));
      itReturnsASemVerIfXIsAValidSemverString('the "version" key');
      itThrowsAnErrorIfXIsNotAValidSemver('the "version" key');
    });
    itThrowsAnErrorIfRawIs('a JSON/YAML number.', '1.0', '1', '-1', '1E4');
    itThrowsAnErrorIfRawIs('a JSON/YAML array.', '["1.2.3"]', '- 1.2.3\n- 3.2.1');
    itThrowsAnErrorIfRawIs('a JSON/YAML boolean.', 'true', 'false');
  });
});
