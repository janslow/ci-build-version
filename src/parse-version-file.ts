import { SemVer, valid as isValidSemVer } from 'semver';
import { readFileSync, readFileAsync } from 'fs-extra-promise';
import { parse as parseYAML } from 'yamljs';

import { parseVersionFile as debug } from './debug';
import { homepage as packageHomepage } from '../package.json';

function tryParseYAML(raw: string): any | undefined {
  try {
    return parseYAML(raw);
  }
  catch (err) {
    debug('Failed to parse YAML.', err);
    return undefined;
  }
}

function tryParseJSON(raw: string): any | undefined {
  try {
    return JSON.parse(raw);
  }
  catch (err) {
    debug('Failed to parse JSON.', err);
    return undefined;
  }
}

export function loadSync(filename: string, encoding: string): SemVer {
  return parse(readFileSync(filename, encoding));
}

export async function load(filename: string, encoding: string): Promise<SemVer> {
  const contents = await readFileAsync(filename, encoding);
  return parse(contents);
}

export function parse(raw: string): SemVer | never {
  if (isValidSemVer(raw)) {
    debug(`Raw file contents ("${raw}") is a valid semantic version.`);
    return new SemVer(raw);
  }
  debug('Failed to parse file as a semantic version.');
  const yaml = tryParseJSON(raw) || tryParseYAML(raw);
  if (typeof yaml === 'string') {
    debug('Parsing YAML string as semantic version...');
    return new SemVer(yaml);
  }
  else if (typeof yaml === 'object') {
    const { version } = yaml;
    if (version !== undefined) {
      debug(`Parsing "version" property of YAML object as semantic version...`);
      return new SemVer(`${version}`);
    }
  }

  throw new Error(`Failed to parse base version from version-file. See ${packageHomepage} for accepted formats.`);
}
