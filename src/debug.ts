import createDebug = require('debug');

import { name as packageName } from '../package.json';

const BASE_NAME = packageName.split('/').pop() as string;

/** @internal */
export const cli = createDebug(`${BASE_NAME}:cli`);

/** @internal */
export const generate = createDebug(`${BASE_NAME}:generate`);

/** @internal */
export const parseVersionFile = createDebug(`${BASE_NAME}:parse-version-file`);
