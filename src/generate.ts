import loadContext, { Variables as Context } from 'gitlab-ci-variables';
import { SemVer, valid as isValidSemVer } from 'semver';
import assert = require('assert');

import { generate as debug } from './debug';

const MAIN_REF_NAMES = [/^master$/, /^develop$/];
const RC_REF_NAMES = [/^hotfix\//, /^release\//];

/**
 * @param base Base version (prerelease is ignored).
 * @returns Semantic version with updated prerelease based on CI context.
 */
export default function generateBuildVersion(base: SemVer): SemVer {
  assert(typeof base === 'object', '`base` must be an object.');
  debug(`Generating build version (from base ${base.toString()}).`);
  return generate(base, loadContext());
}

/** @internal */
export function generate(base: SemVer, context: Context | undefined): SemVer {
  const version = new SemVer(base.toString());
  if (!context) {
    debug(`Running locally (GitLab CI not detected).`);
    version.prerelease = ['local'];
  } else if (context.build.tag && isValidSemVer(context.build.tag)) {
    debug(`Running on tagged release ("${context.build.tag}").`);
    version.prerelease = [];
    const v = new SemVer(context.build.tag);
    assert(version.compare(v) === 0, `Tag ("${context.build.tag}") must match base version (ignoring prerelease).`);
  } else if (isReleaseBranch(context)) {
    debug(`Running on release branch ("${context.build.refName}").`);
    version.prerelease = sanitizePrerelease('rc', context.pipeline.id);
  } else if (isMainBranch(context)) {
    debug(`Running on main branch ("${context.build.refName}").`);
    version.prerelease = sanitizePrerelease('dev', context.pipeline.id);
  } else {
    debug(`Running on unknown ref ("${context.build.refName}").`);
    version.prerelease = sanitizePrerelease('ci', context.build.refName, context.pipeline.id);
  }
  return new SemVer(version.format());
}

function sanitizePrerelease(...parts: (string|number)[]): string[] {
  return parts
    .map(x => `${x}`)
    .map(x => x.split(/[\/\-\.]/))
    .reduce((xs, ys) => xs.concat(ys), [])
    .map(x => x.replace(/[^a-zA-Z0-9]/, '').replace(/^0+/, ''))
    .filter(x => x.length > 0);
}

function isBranch(context: Context): boolean {
  return !context.build.tag;
}

function isMainBranch(context: Context): boolean {
  return isBranch(context) && MAIN_REF_NAMES.some(x => x.test(context.build.refName));
}

function isReleaseBranch(context: Context): boolean {
  return isBranch(context) && RC_REF_NAMES.some(x => x.test(context.build.refName));
}
