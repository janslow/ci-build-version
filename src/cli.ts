#!/usr/bin/env node

import { SemVer, valid as isValidSemVer } from 'semver';
import yargs = require('yargs');

import { version as packageVersion } from '../package.json';
import { cli as debug } from './debug';
import generateBuildVersion from './generate';
import { loadSync as loadVersionFile } from './parse-version-file';

interface Args {
  'base-version'?: SemVer;
  'version-file'?: SemVer;
}

function parseBaseVersion(raw: string): SemVer | never {
  debug(`Parsing "${raw}" as a semver...`);
  if (isValidSemVer(raw)) {
    return new SemVer(raw);
  }
  throw new Error(`base-version must be a valid semantic version (http://semver.org/).`);
}

function run(): void {
  const args = yargs
    .option('base-version', {
      alias: 'b',
      coerce: parseBaseVersion,
      description: 'Base version to generate build version from.',
      requiresArg: true,
      string: true,
      group: 'Base Version Options',
    })
    .option('version-file', {
      alias: 'f',
      coerce: path => loadVersionFile(path, 'UTF8'),
      conflicts: 'base-version',
      description: 'YAML/JSON file to read base version from. See README.md.',
      requiresArg: true,
      string: true,
      group: 'Base Version Options',
    })
    .example('$0 -b 1.2.3-local', 'Use the base version "1.2.3-local"')
    .example('$0 -f package.json', 'Use the base version in package.json (the "version" key)')
    .example('$0 -f product.version', 'Use the base version in product.version (assuming file just contains the base version)')
    .version(packageVersion)
    .strict()
    .help()
    .argv as Args;
  const baseVersion = args['base-version'] || args['version-file'];
  if (!baseVersion) {
    throw new Error('Either --base-version or --version-file must be used. See usage.');
  }
  debug(`Base Version: ${baseVersion}`);
  const version = generateBuildVersion(baseVersion);
  debug(`Build Version: ${version}`);
  process.stdout.write(version.toString());
}

if (require.main === module) {
  run();
}
