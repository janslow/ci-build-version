import { expect } from 'chai';
import { SemVer } from 'semver';
import { Variables as Context } from 'gitlab-ci-variables';
import { AssertionError } from 'assert';

import { generate } from './generate';

const ANY: any = undefined;

describe('generate', () => {
  describe('generate(baseVersion)', () => {
    function itSetsThePrereleaseTo(prerelease: string, spec?: () => void): void {
      it(`sets the prerelease to "${prerelease}".`, spec);
    }

    let base: SemVer;
    beforeEach(() => {
      base = new SemVer('1.2.3-local');
    });

    describe('outside GitLab CI', () => {
      itSetsThePrereleaseTo('local', () => {
        base = new SemVer('1.2.3-foo');
        expect(generate(base, undefined)).to.deep.equal(new SemVer('1.2.3-local'));
      });
    });
    describe('in GitLab CI', () => {
      function createEmptyContext(pipelineId: number): Context {
        return {
          build: {
            refName: ANY,
            id: ANY,
            manual: ANY,
            name: ANY,
            ref: ANY,
            refSlug: ANY,
            repo: ANY,
            stage: ANY,
            tag: ANY,
            token: ANY,
            triggered: ANY,
          },
          debug: ANY,
          pipeline: { id: pipelineId },
          project: ANY,
          runner: ANY,
          server: ANY,
          user: ANY,

        };
      }
      function createBranchContext(pipelineId: number, branch: string): Context {
        const context = createEmptyContext(pipelineId);
        (context.build as any).refName = branch;
        return context;
      }
      function createTagContext(pipelineId: number, tag: string): Context {
        const context = createEmptyContext(pipelineId);
        (context.build as any).tag = tag;
        (context.build as any).refName = tag;
        return context;
      }
      describe('on a tag ref', () => {
        describe('when tag is a semantic version', () => {
          it('sets the version to the tag.', () => {
            expect(generate(base, createTagContext(1, '1.2.3'))).to.deep.equal(new SemVer('1.2.3'));
            base = new SemVer('3.2.1');
            expect(generate(base, createTagContext(1, '3.2.1'))).to.deep.equal(new SemVer('3.2.1'));
          });
          it('throws an error if the tag is not equal to the base version.', () => {
            expect(() => generate(base, createTagContext(1, '3.2.1'))).to
              .throw(AssertionError, 'Tag ("3.2.1") must match base version (ignoring prerelease).');
          });
        });
        describe('otherwise', () => {
          itSetsThePrereleaseTo('ci.$CLEAN_TAG.$PIPELINE_ID', () => {
            expect(generate(base, createTagContext(1, '1.2'))).to.deep.equal(new SemVer('1.2.3-ci.1.2.1'));
            expect(generate(base, createTagContext(1, 'foo'))).to.deep.equal(new SemVer('1.2.3-ci.foo.1'));
            expect(generate(base, createTagContext(1, 'foo/bar'))).to.deep.equal(new SemVer('1.2.3-ci.foo.bar.1'));
            expect(generate(base, createTagContext(1, '0'))).to.deep.equal(new SemVer('1.2.3-ci.1'));
            expect(generate(base, createTagContext(1, '0a'))).to.deep.equal(new SemVer('1.2.3-ci.a.1'));
            expect(generate(base, createTagContext(1, '1.2.3.4'))).to.deep.equal(new SemVer('1.2.3-ci.1.2.3.4.1'));
          });
        });
      });
      describe('on a branch ref', () => {
        describe('when branch is "develop"', () => {
          itSetsThePrereleaseTo('dev.$PIPELINE_ID', () => {
            expect(generate(base, createBranchContext(1357, 'develop'))).to.deep.equal(new SemVer('1.2.3-dev.1357'));
          });
        });
        describe('when branch is "master"', () => {
          itSetsThePrereleaseTo('dev.$PIPELINE_ID', () => {
            expect(generate(base, createBranchContext(7531, 'master'))).to.deep.equal(new SemVer('1.2.3-dev.7531'));
          });
        });
        describe('when branch starts with "release/"', () => {
          itSetsThePrereleaseTo('rc.$PIPELINE_ID', () => {
            expect(generate(base, createBranchContext(1, 'release/'))).to.deep.equal(new SemVer('1.2.3-rc.1'));
            expect(generate(base, createBranchContext(2, 'release/1.2.3'))).to.deep.equal(new SemVer('1.2.3-rc.2'));
            expect(generate(base, createBranchContext(3, 'release/3.2.1'))).to.deep.equal(new SemVer('1.2.3-rc.3'));
            expect(generate(base, createBranchContext(4, 'release/foo'))).to.deep.equal(new SemVer('1.2.3-rc.4'));
          });
        });
        describe('when branch starts with "hotfix/"', () => {
          itSetsThePrereleaseTo('rc.$PIPELINE_ID', () => {
            expect(generate(base, createBranchContext(1, 'hotfix/'))).to.deep.equal(new SemVer('1.2.3-rc.1'));
            expect(generate(base, createBranchContext(2, 'hotfix/1.2.3'))).to.deep.equal(new SemVer('1.2.3-rc.2'));
            expect(generate(base, createBranchContext(3, 'hotfix/3.2.1'))).to.deep.equal(new SemVer('1.2.3-rc.3'));
            expect(generate(base, createBranchContext(4, 'hotfix/foo'))).to.deep.equal(new SemVer('1.2.3-rc.4'));
          });
        });
        describe('otherwise', () => {
          itSetsThePrereleaseTo('ci.$CLEAN_BRANCH.$PIPELINE_ID', () => {
            expect(generate(base, createBranchContext(1, '1.2'))).to.deep.equal(new SemVer('1.2.3-ci.1.2.1'));
            expect(generate(base, createBranchContext(1, 'foo'))).to.deep.equal(new SemVer('1.2.3-ci.foo.1'));
            expect(generate(base, createBranchContext(1, 'foo/bar'))).to.deep.equal(new SemVer('1.2.3-ci.foo.bar.1'));
            expect(generate(base, createBranchContext(1, 'feature/bar'))).to.deep.equal(new SemVer('1.2.3-ci.feature.bar.1'));
            expect(generate(base, createBranchContext(1, '0'))).to.deep.equal(new SemVer('1.2.3-ci.1'));
            expect(generate(base, createBranchContext(1, '0a'))).to.deep.equal(new SemVer('1.2.3-ci.a.1'));
            expect(generate(base, createBranchContext(1, '1.2.3.4'))).to.deep.equal(new SemVer('1.2.3-ci.1.2.3.4.1'));
          });
        });
      });
    });
  });
});
