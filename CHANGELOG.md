# Changelog

## develop
* ~breaking Prevent both `--base-version` and `--version-file` options being used.

## 0.1.1
* Remove production dependencies on TS definitions.

## 0.1.0
* Initial release.
