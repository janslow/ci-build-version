interface Package {
  name: string;
  version: string;
  homepage: string;
}
declare const pkg: Package;
export = pkg;
